# Pascaligo VSCODE Plugin

This repository contains syntax highlighting, and the language server/client for the pascaligo grammar. 

## Functionality

At this time the language server depends on a remote service called the [lspdriver](https://gitlab.com/ligolang/lspdriver) to generate parse errors and dryrun events. 

## Structure

```
.
├── syntaxes // Textmate Grammar
├── client // Language Client
│   ├── src
│   │   ├── test // End to End tests for Language Client / Server
│   │   └── extension.ts // Language Client entry point
├── package.json // The extension manifest.
└── server // Language Server
    └── src
        └── server.ts // Language Server entry point
```

